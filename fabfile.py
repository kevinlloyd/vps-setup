from __future__ import with_statement
from fabric.api import *

# env.hosts = ['198.58.116.65:55911']
env.hosts = ['198.58.116.65']
env.user = 'kevin'
home = ''

vhost = """
cat <<EOF | sudo tee /etc/apache2/sites-available/{site}
# domain: {site}
# public: {home}/public/{site}/www/

<VirtualHost *:80>
	# Admin email, Server Name (domain name), and any aliases
	ServerAdmin webmaster@{site}
	ServerName  {site}
	ServerAlias www.{site}

	# Index file and Document Root (where the public files are located)
	DirectoryIndex index.php index.html
	DocumentRoot {home}/public/{site}/www
	Options -Indexes FollowSymLinks

	# Log file locations
	LogLevel warn
	ErrorLog  {home}/public/{site}/logs/error.log
	CustomLog {home}/public/{site}/logs/access.log combined
	
	<IfModule mpm_itk_module>
		AssignUserId {user} {user}
	</IfModule>
</VirtualHost>
EOF
"""
@task
def add_site(name, user):
	"""	Adds a site to the  web server. (name, user)"""
	with settings(warn_only=True):
		""" Don't abort on error. """

		home = '/home/{0}'.format(user) # Set the home folder

		# Set parameters to use in string.format() commands
		params = {'home': home, 'user': user, 'site': name}
		
		
		out = run('id ' + user) # Test if user exists
		if out.return_code == 1:
			add_user(user)

		make_directories(params)
		setup_apache(params)
	pass

def setup_apache(params):
	sudo(vhost.format(**params))
	sudo('a2ensite {site}'.format(**params))
	sudo('service apache2 reload')

def make_directories(params):
	sudo('mkdir {home}/public/'.format(**params))
	sudo('mkdir -p {home}/public/{site}/{{www,logs}}'.format(**params))
	sudo('chmod a+x {home}'.format(**params)) # Apache needs executable permissions in the home folder for some reason
	sudo('chmod -R a+rx {home}/public/{site}/www'.format(**params))
	sudo('echo Welcome to {site}! > {home}/public/{site}/www/index.html'.format(**params))
	
	sudo('chown -R {user}:{user} {home}/public'.format(**params))

@task
def add_user(user):
	""" Add a user to the system. 
		Note this relies on appropriate settings in /etc/adduser.conf 'change DIR_MODE=0750 or 700'
		Also /etc/skel will contain default values
	"""
	print 'Adding new user: ' + user
	sudo('adduser ' + user)

@task
def create_db(rUser, rPassword, dbName):
	""" Creates a new database name. (rUser, rPassword, dbName) """
	run('mysql -u{0} -p{1} -e "create database {2}"'.format(rUser, rPassword, dbName))

@task
def create_db_user(rUser, rPassword, dbName, dbUser, dbPwd):
	""" Creates a new user for the given database (rUser, rPassword, dbName, dbUser, dbPwd) """
	cmd = 'mysql -u{0} -p{1} -e "grant all on {2}.* to \'{3}\' identified by \'{4}\';" FLUSH PRIVILEGES;'
	run(cmd.format(rUser, rPassword, dbName, dbUser, dbPwd))

@task
def transfer_site(site, database):
	""" Will transfer the file and database from HostMonster to Linode (site, database) """
	with settings(warn_only=True):
		with cd('~/transfer'):
			run('./transfer.sh {0} {1}'.format(site, database))
