# Linode VPS - Ubuntu Setups

This project will track setup for the Linode VPS. This will mainly be a LAMP server, with the occasional Django application.

Project consists of setup notes and instructions for the Linode VPS and a [Fabric](http://docs.fabfile.org/en/1.6/) file the helps with regular server tasks
